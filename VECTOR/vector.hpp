#ifndef _VECTOR_H
#define _VECTOR_H

class vector{
    public:
        typedef long long int ssize_t;
        typedef long long int index_t;
        typedef enum { SUCCESS=1, INDEX_OUT_OF_RANGE,
                        INVALID_SIZE,VECTOR_EMPTY} status_t;

        // default constructor
        vector(); 
        //vector with pre-allocation
        vector(ssize_t N); 
        // prealloc with initial value
        vector(ssize_t N, int value);
        // init by other vector
        vector(const vector& other_vec);

        ~vector();

        //member functins
        ssize_t size() const;
        ssize_t max_size() const;

        status_t push_back(int new_value);
        status_t pop_back(int* p_data);

        status_t get(index_t index, int* p_data);
        status_t set(index_t index, int new_value);

        void show(const char* msg) const;
        void sort();

        index_t search(int search_element) const;

        index_t search( int search_element,
                        index_t start_index,
                        index_t end_index) const;

        // overloaded operators
        void operator=(vector &src);
        bool operator==(vector &op2);
        // TODO : operator+ throws expection when both operands are of different size 
        [[nodiscard("Potential memory leak")]] vector& vector::operator+(vector &op2);

    private:
        int* arr;
        ssize_t N;
        bool freeme; 
    
};

#endif
