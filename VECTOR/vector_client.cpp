#include <iostream>
#include <cstdlib>
#include "vector.hpp"
#include "cassert"

void validate_get_set_method(void);
void validate_search(void);
void validate_sort(void);
void validate_copy(void);
void validate_addition(void);

int main(void) {
    vector v1;
    vector v2(5);
    vector v3(5,30);
    vector v4(v3);

    v1.show("v1: ");
    v2.show("v2: ");
    v3.show("v3: ");
    v4.show("v4: ");

    std::cout << "V4 size:" << v4.size() << ","  << "V4 max size: " << v4.max_size() << std::endl;

    for (int index = 0; index < 3; index++) {
        v1.push_back(index*5);
    }
    v1.show("V1 is :");
    
    int data;
    std::cout << "Poped elements from V1:" << std::endl;
    while(v1.pop_back(&data) != vector::VECTOR_EMPTY) {
        std::cout << data << std::endl;
    }

    validate_get_set_method();
    validate_search();
    validate_sort();
    validate_copy();
    validate_addition();
}

void validate_addition(void) {

    std::cout << "---------------------------VALIDATE ADDITION Case 1---------------------" << std::endl;
    vector Res, Op1, Op2, Op3(10, 5);
    for(int index=0; index < 10; index++) {
        Op1.push_back(index*5);
    }
    Op1.show("Op1 is:");
    for(int index=0; index < 10; index++) {
        Op2.push_back(index*5+1);
    }
    Op2.show("Op2 is:");

    Res = Op1 + Op2;
    Res.show("Res is:");

    std::cout << "---------------------------VALIDATE ADDITION Case 2----------------------" << std::endl;
    Res = Op1 + Op2 + Op3;
    Res.show("Res is:");

    std::cout << "---------------------------VALIDATE ADDITION Case 3----------------------" << std::endl;
    vector *pRes = 0;
    vector *pOp1 = new vector(10, 5);
    vector *pOp2 = new vector(10, 3);
    vector *pOp3 = new vector(10, 2);
    pRes = new vector(*pOp1 + *pOp2);
    std::cout << "result copied to: " << pRes << std::endl;
    pRes->show("pRes is:");

    std::cout << "---------------------------VALIDATE ADDITION Case 4----------------------" << std::endl;
    vector Res2(*pOp1 + *pOp2 + *pOp3);
    Res2.show("Res2 is:");

     std::cout << "---------------------------VALIDATE ADDITION Case 5----------------------" << std::endl;
     Op1 + Op2;
     *pOp1 + *pOp2 + *pRes;
}

void validate_copy(void) {

    std::cout << "-----------------------VALIDATE COPY AND COMPARISION---------------" << std::endl;
    vector Src, Dst, Dst2;
    vector *pSrc = new vector(10);
    vector *pDst, *pDst2;
    
    for(int index=0; index < 10; index++) {
        Src.push_back(index*5);
    }
    Src.show("Src is :");
    //printf("Address of src:%p, Address of Dst:%p\n", &Src, &Dst);
    Dst = Src;
    Dst.show("Dst is :");
    assert((Dst == Src));


    for(int index=0; index < 10; index++) {
        pSrc->push_back(index*3);
    }

    pSrc->show("pSrc is :");
    pDst = pSrc;    
    pDst->show("pDst is :");
    assert(pDst == pSrc);

    Dst2 = *pDst;
    Dst2.show("Dst2 is :");

    pDst2 = new vector;
    *pDst2 = Dst2;
    pDst2->show("pDst2 is :");
    assert(*pDst2 == Dst2);
}


void validate_get_set_method() {

std::cout << "-----------------------VALIDATE GET-SET-----------------------" << std::endl;
    vector* vp = new vector(10);
    int data = -1;

    for(int index=0; index < 10; index++) {
        vp->set(index, index*5);
        vp->get(index, &data);
        if(data != index*5) {
            std::cout << "Exiting get set logic failed" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::cout << "Get Set works fine unless....." << std::endl;
}


void validate_search() {
    std::cout << "-----------------------VALIDATE SEARCH-------------------" << std::endl;
    vector* vp = new vector(10);
    int data = -1;

    for(int index=0; index < 10; index++) {
        vp->set(index, index*5);
    }

    //vp->show("Search Database");

    if(vp->search(45) != 9) {
            std::cout << "1: Exiting search logic failed " << std::endl;
            exit(EXIT_FAILURE);
    }

    if(vp->search(45, 9, 9) != 9) {
            std::cout << "2: Exiting search logic failed" << std::endl;
            exit(EXIT_FAILURE);
    }

    if(vp->search(0, 0, 0) != 0) {
            std::cout << "3: Exiting search logic failed" << std::endl;
            exit(EXIT_FAILURE);
    }

    if(vp->search(25, 0, 7) != 5) {
            std::cout << "4: Exiting search logic failed" << std::endl;
            exit(EXIT_FAILURE);
    }
    std::cout << "Search works fine unless....." << std::endl;
}


void validate_sort(void) {
    std::cout << "-----------------------VALIDATE SORT-------------------" << std::endl;
    vector* vp = new vector(1000);

    srand((unsigned) time(NULL));

    int random = rand();

    for(int index=0; index < 1000; index++) {
        vp->set(index, random);
        random = rand();
    }

    vp->sort();

    int temp = 0;
    for(int index=0; index < 1000; index++) {
        vp->get(index, &random);

        if(random < temp) {
            std::cout << "Exiting sort logic failed" << std::endl;
            exit(EXIT_FAILURE);
        }
        temp = random;
    }
    std::cout << "Sort works fine unless....." << std::endl;
}