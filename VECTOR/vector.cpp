#include "vector.hpp"
#include <iostream>
#include <cmath>

static vector::ssize_t partition(int * arr, vector::ssize_t low, vector::ssize_t high) {
    /* select last element is pivot*/
    int pivot, temp;
    
    pivot = arr[high];
    vector::ssize_t pivot_idx = low-1;
    for(vector::ssize_t index = low; index < high; index++) {
        if( arr[index] <= pivot ) {
            pivot_idx++;
            temp = arr[pivot_idx]; 
            arr[pivot_idx] = arr[index];
            arr[index] = temp;
        }
    }

    pivot_idx++;
    temp = arr[high];
    arr[high] = arr[pivot_idx];
    arr[pivot_idx] = temp;

    return pivot_idx;
}

static void quicksort(int *arr, vector::ssize_t low, vector::ssize_t high)
{
    vector::ssize_t pivot_index;
    // Validate Index
    if((low >= high) || (low < 0)){
        return;
    }

    // partition
    pivot_index = partition(arr, low, high);

    //sort the partitions
    quicksort(arr, low, pivot_index -1 );
    quicksort(arr, pivot_index+1, high);
}

vector::vector(): arr(0), N(0){

}

vector::vector(ssize_t _N) {
    if( _N <= 0){
        arr = 0;
        N = 0;
    } else {
        N = _N;
        arr = (int*)calloc(N, sizeof(int));
        if(arr == 0){
            std::cout << "Fatal : out of memory" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    freeme = false;
}

vector::vector(ssize_t _N, int value){
    if( _N <= 0){
        arr = 0;
        N = 0;
    } else {
        N = _N;
        arr = (int*)malloc(N*sizeof(int));
        if(arr == 0){
            std::cout << "Fatal : out of memory" << std::endl;
            exit(EXIT_FAILURE);
        }

        for(index_t index  = 0; index < N; index++) {
            arr[index] = value;
        }
    }
    freeme = false;
}
// init by other vector
vector::vector(const vector& other_vec){
    N = other_vec.N;
    arr = (int*) malloc(N*sizeof(int));
    if(arr == 0){
        std::cout << "Fatal : out of memory" << std::endl;
        exit(EXIT_FAILURE);
    }
    memcpy(arr, other_vec.arr, N*sizeof(int));
    freeme = false;
    if(other_vec.freeme == true) {
        std::cout << "Temp chained object consumed , freeing op2 : " << &other_vec << std::endl; // TODO: Delete me 
        delete &other_vec;
    }
}

//member functins
vector::ssize_t vector::size() const{
    return N;
}

vector::ssize_t vector::max_size() const{
    ssize_t m_size = -1; 
    int bits = (8*sizeof(void *)) - 1;
    m_size = (ssize_t)(pow(2, bits) - 1);
    return (m_size); 
}


vector::status_t vector::push_back(int new_value){
    arr = (int *)realloc(arr, (N+1)*sizeof(int));
    if(arr == 0) {
        std::cout << "fatal:Error in growing array" << std::endl;
        exit(EXIT_FAILURE);
    }
    N = N+1;
    arr[N-1] = new_value;
    return SUCCESS;
}

vector::status_t vector::pop_back(int* p_data){
    if(N == 0)
        return (VECTOR_EMPTY);
    *p_data = arr[N-1];
    if((N-1) == 0) {
        free(arr);
        arr = 0;
        N = 0;
    } else {
        arr = (int *)realloc(arr, (N-1)*sizeof(int));
        if(arr == 0) {
            std::cout << "fatal:Error in shrinking array" << std::endl;
            exit(EXIT_FAILURE);
        }
        N = N-1;
    }
    return SUCCESS;
}


vector::status_t vector::get(index_t index, int* p_data)
{
    status_t status = SUCCESS;
    if( N == 0) {
        status = VECTOR_EMPTY;
    } else if((index < N) && (index >= 0)) {
        *p_data = arr[index];
    } else {
        status = INDEX_OUT_OF_RANGE;
    }
    return status;
}

vector::status_t vector::set(index_t index, int new_value){
    
    status_t status = SUCCESS;
    if( N == 0) {
        status = VECTOR_EMPTY;
    } else if((index < N) && (index >= 0)) {
        arr[index] = new_value;
    } else {
        status = INDEX_OUT_OF_RANGE;
    }

    return status;
}


void vector::operator=(vector &src) {
    int temp = 0;
    std::cout << "Overloaded assignment of this:" << this << " with Src :" << &src << std::endl; // TODO : Delete me
    for(index_t index = 0; index < src.size(); index++){

        if( src.get(index, &temp) == INDEX_OUT_OF_RANGE ) {
            std::cout << "fatal:Index out of range" << std::endl;
            exit(EXIT_FAILURE);
        }
        this->push_back(temp);
    }
    if(src.freeme == true){
        std::cout << "Vector copied , freeing the temporary source : " << &src << std::endl;
        delete &src;
    }
    //this->show("Inside assignment\n");  
}

bool vector::operator==(vector &op2) {
    bool result = false;
    int temp_data;
    std::cout << "Overloaded comparision of this:" << this << " with Src :" << &op2 << std::endl; // TODO : Delete me
    // compare only if both vectors are of same size 
    if(N == op2.size()) {
        for(index_t index = 0; index < op2.size(); index++){

            if( op2.get(index, &temp_data) == INDEX_OUT_OF_RANGE ) {
                std::cout << "fatal:Index out of range" << std::endl;
                exit(EXIT_FAILURE);
            }
            if( temp_data != arr[index]) {
                break;
            } else if (index == (op2.size()-1)) {
                result = true;
            }
        }
    }
    return result;
    //this->show("Inside comparision\n");  
}

[[nodiscard("Potential memory leak")]] vector& vector::operator+(vector &op2) {
    int temp = 0;
    vector* p_result = 0;
    std::cout << "\nOverloaded addition of this:" << this << " with op2 :" << &op2 << std::endl; // TODO : Delete me
    // add  only if both vectors are of same size 
    if(N == op2.size()) {
        p_result = new vector();
        // indication to free result vector once it is copied / used
        p_result->freeme = true;
        std::cout << "Placting result in location:" << p_result << std::endl;  // TODO: Delete me 

        for(index_t index = 0; index < op2.size(); index++){

            if( op2.get(index, &temp) == INDEX_OUT_OF_RANGE ) {
                std::cout << "fatal:Index out of range" << std::endl;
                exit(EXIT_FAILURE);
            }
            p_result->push_back(temp + arr[index]);
        }
    } else {
        // TODO : Throw exception here , client must catch it                
        std::cout << "fatal:Both operands must be of same size for addition" << std::endl;
        exit(EXIT_FAILURE);
    }
    // In chained addition we need to free temporary object
    if(op2.freeme == true) {
        std::cout << "Temp chained object consumed , freeing op2 : " << &op2 << std::endl; // TODO: Delete me 
        delete &op2;
    }
    if(this->freeme == true) {
        std::cout << "Temp chained object consumed , freeing this : " << this << std::endl;// TODO: Delete me 
        delete this;
    }
    return *p_result;
    //this->show("Inside assignment\n");  
}

void vector::sort() {
    quicksort(arr, 0, N-1);
}


vector::index_t vector::search( int search_element,
                index_t start_index,
                index_t end_index) const {

    /*validate index*/
    if( start_index < 0 ||
        start_index > end_index ||
        end_index >= N ) {
            //std::cout << "Index validation Failed in search" << std::endl;
            return -1;
    }

    for(index_t index = start_index; index <= end_index; ++index) {
        if(arr[index] == search_element) {
            //std::cout << "Item found at :" << index << std::endl;
            return index;
        }
    }
    //std::cout << "Search element not found" << std::endl;
    return -1;

}


vector::index_t vector::search(int search_element) const{

    return search(search_element, 0, N-1);
}



void vector::show(const char *msg) const{
    std::cout << msg << std::endl;
    for(index_t index  = 0; index < N; index++) {
        std::cout << "Element[" << index << "]" << arr[index] << " ," ;
    }
    std::cout << std::endl;
}


vector::~vector() {
    if(arr) {
        free(arr);
        arr = 0;
    }
}
